﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class TasskTest : MonoBehaviour
{
    void Start()
    {
        Shet(10);
        //Task tr = new Task(() =>
        //{
        //    Factorial(4);
        //});
        //tr.Start();
        Task tr2 = Task.Factory.StartNew(() =>
        {

        });
        tr2.Wait();
        Debug.Log("Task Finished");
        //Task tr3 = Task.Run(() =>
        //{
        //    Factorial(10);
        //});
    }
    public void Factorial(int b)
    {
        int a = 1;
        for (int i = 1; i <= b; i++)
        {
            a = a * i;
        }
        Debug.Log(a);
    }
    public void Chisla(int a)
    {
        for (int j = 0; j <= a; j++)
        {
            Debug.Log(j);
            Thread.Sleep(1000);
        }
    }
    public void Shet(int N)
    {
        Task tr4 = Task.Run(() =>
        {
            Chisla(N);
        });
        tr4.ContinueWith((task) =>
        {
            Chisla(N * 2);
        });
    }    
}